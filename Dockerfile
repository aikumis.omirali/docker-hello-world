FROM python:3.9.2

COPY app.py .

CMD [ "python3", "app.py"]
